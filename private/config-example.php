<?php
/**
 * At a minimum, insert a hook_secret string here to secure your installation
 */
return [
  'debug'         => false,  // (bool) true || false
  'hook_secret'   => '',     // <-- (string) insert hook secret here !!!
  'satis_command' => [
    'command'          => 'build',
    'file'             => __DIR__ . '/satis.json',
    'output-dir'       => dirname( __DIR__ ) . '/public_html',
  ],
];
